#!/bin/bash

n=8
while [ $n -le 1025 ]
do	
size="${n}x${n}"

ffmpeg -f rawvideo -video_size $size -pixel_format yuv420p -framerate 25 -i /dev/urandom -an -t 5 -vf hue=s=0 outputb${n}.mkv
ffmpeg -f rawvideo -video_size $size -pixel_format yuv420p -framerate 25 -i /dev/urandom -an -t 5 output${n}.mkv
ffmpeg -i outputb${n}.mkv -vf "drawtext=text='*':fontcolor=black:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2" -an -vcodec libx264 -crf 30 -keyint_min 249 -bf 0 -threads 0 outputb${n}.mp4
ffmpeg -i output${n}.mkv -vf "drawtext=text='II':fontcolor=black:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2" -an -vcodec libx264 -crf 30 -keyint_min 249 -bf 0 -threads 0 output${n}.mp4
n=$((n*2))
done
