**forty seconds of pause**

five seconds stolen from the infinite scroll continuum.

five seconds of relief from the dopamine addiction.

five seconds without looking at irrealistic expectations and non-existing persons.

five seconds ignoring smarter people, better people, people more popular.

five seconds not counting the numbers of interactions, responses and other forms of quantification.

five seconds doubting the social assumption that you HAVE to be on this platform, as an artist / designer / ...

five seconds questioning the system of profiling, of advertisements, of hidden and visible product placement.

five seconds of less toxic types of procrastination.
