**forty seconds of noise**

five seconds observing video optimization at work.

five seconds wondering how something is processed by algorhythms inside a black box.

five seconds admiring the artifacts of compression and conversion.

five seconds concentrating on just one infinitely small turning wheel part of a very complex mechanism.

five seconds appreciating noise, something apparently too expensive and too random to simplify and make more efficient.

five seconds looking at nothing, except maybe the frame that contains, regulates, processes, normalizes that nothing.

five seconds of studying a user-friendly system, something that tries to disappear from your perception all the time.

five seconds searching for a handle, trying to approach something just too complex to understand.
