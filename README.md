# pause-noise

Files are in the [noise](noise) and [pause](pause) folders.

![](noise/06_output256.mp4) ![](pause/06_output256.mp4)

## code used to generate the noise patterns

The files are created with an open-source software called FFmpeg. It allows to create modify and process video from the terminal.\

First a noise pattern is created, using the random source of noise in /dev/urandom:

`ffmpeg -f rawvideo -video_size 8x8 -pixel_format yuv420p -framerate 25 -i /dev/urandom -an -t 5 output.mkv`

Then the video is converted to the more glitch-prone mp4 format, adding a text overlay:

`ffmpeg -i output$.mkv -vf "drawtext=fontfile=/path/to/font.ttf:text='*':fontcolor=black:fontsize=48:boxborderw=5:x=(w-text_w)/2:y=(h-text_h)/2" -an -vcodec libx264 -crf 30 -keyint_min 249 -bf 0 -threads 0 output.mp4`

To automate this for different sizes of the pattern, the file [auto.sh](auto.sh) generates the patterns for different pixels size.


## how to install ffmpeg in OSX

In your Terminal, first install the XCODE tools to your system, typing:

`xcode-select --install`

Then download and install the Homebrew package system for OSX, by pasting in:

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`

Finally, install FFMpeg:

`brew install ffmpeg`

Now you can check the command's options...

`ffmpeg --help`

Or lookup on the internet for the many potential uses

[`https://codingpoint.tech/most-useful-ffmpeg-commands/`](https://codingpoint.tech/most-useful-ffmpeg-commands/)
